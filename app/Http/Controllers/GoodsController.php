<?php

namespace App\Http\Controllers;

use App\Good;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Mockery\CountValidator\Exception;
use Monolog\Logger;

class GoodsController extends Controller
{

    public function create()
    {
        return view('goods/create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required|numeric',
            'image' => 'image'
        ]);

        try {

            $file = $request->file('image');
            $file->getRealPath();
            $file->move('asd', $file->getClientOriginalName());

            Log::notice('Успех записи');
            $good = new Good();
            $good->name = $request->input('name');
            $good->price = $request->input('price');
            //в базу
            $good->user_id = Auth::user()->id;

            $good->save();
        } catch (Exception $e) {
            Log::error('Ошибка записи');
            return redirect('/home');
        }
    }

    public function edit($id)
    {

    }

    public function update($id)
    {

    }

    public function index()
    {
        $goods = Good::with('userdata')->get();
        $data['goods'] = $goods;
        return view('goods/index', $data);
    }

    public function destroy($id)
    {

    }
}
