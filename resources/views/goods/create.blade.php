@extends('layouts.app')

@section('content')
    @if(!empty($errors->all()))
        @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
        @endforeach
    @endif
    <form action="/goods/store" method="post" enctype="multipart/form-data">
        <input type="text" name="name">
        <input type="text" name="price">
        <input type="file" name="image">
        <input type="submit">
        {{csrf_field()}}
    </form>
@endsection